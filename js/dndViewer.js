	function DisplayCharacterArray()
	{
		var stringBuilder = "<div class='table-responsive text-center'><table class='table text-center;'><tr><th>Name</th><th>Afflictions</th><th>Held Turn</th></tr>";
		var availChar = GetAvailableCharacterArray();
		for(var i = 0; i < availChar.length; i++)
		{
			stringBuilder += "<tr" + (availChar[i].CurrentTurn ? " class='current'" : "")  + " id='row" + availChar[i].ID + "' ><td>" + availChar[i].Name  + (availChar[i].TokenNumber <= 0 || availChar[i].TokenNumber == null ? "" : "{" + availChar[i].TokenNumber + "}") +"</td>";
			stringBuilder += "<td>";
			for(var j = 0; j < availChar[i].Afflictions.length; j++)
				stringBuilder += "<span data-toggle='tooltipHTML' class='bsTooltip' data-placement='bottom' data-original-title='" + availChar[i].Afflictions[j].Tooltip + "'>" + availChar[i].Afflictions[j].Name + "</span><br>" ;	
		stringBuilder += "</td><td>" + (availChar[i].TurnHeld ? "Yes": "No") + "</td></tr>";
		
		}
		stringBuilder += "</table></div>";
		$("#" + POPULATEID).html(stringBuilder);
		ResetTooltip();
		return false;
	}
		//This function uses NOTY
	function DisplayMessages(clearMessagesAfter)
	{
		var messages = GetMessages();
		if(messages.length == 0)
			return;
		var notificationAllowed = GetNotificationsAreAllowed();
		DisplayLoop(messages, 0, notificationAllowed);                    
		if(clearMessagesAfter == true)
			ClearLocalStorageItem(MSGITEM);
	}
	function DisplayLoop (messages, i, notificationAllowed) 
	{      
		   if(i == messages.length)
		   {
			   ScrollToCurrentTurn();
		   }
		   else
		   {
			   if((notificationAllowed && messages[i].Message) || messages[i].IsSystemMessage)
			   {
					ShowNotification(messages[i].Type, messages[i].Message, true);
			   }  
			  i++;
			  if (i <= messages.length) DisplayLoop(messages, i, notificationAllowed);  
		   }  
	};