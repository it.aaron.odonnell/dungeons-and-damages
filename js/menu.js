	var MenuLinks = [
	{Name: "Home", Link: "index.html", NewWindow: false},
	{Name: "Characters", Link: "setup.html", NewWindow: false},
	{Name: "Viewer", Link: "viewer.html", NewWindow: true},
	//{Name: "Magic Items", Link: "loot.html", NewWindow: false},
	{Name: "Help", Link: "help.html", NewWindow: false},
	{Name: "Site Developer", Link: "sitedev.html", NewWindow: false},
	{Name: "Feedback", Link: "feedback.html", NewWindow: false},
	];
	function PopulateLinks(ulElmID)
	{
		var windowLocation = window.location.href;
		var strMenuHtml = "";
		for(var i = 0; i < MenuLinks.length; i++)
		{
			strMenuHtml += "<li" + (windowLocation.indexOf(MenuLinks[i].Link) >= 0 ? " class='active' " : "") + ">";
			strMenuHtml += "<a " + (MenuLinks[i].NewWindow == true ? "target='_blank' " : "") + "href='" + MenuLinks[i].Link + "'>" + MenuLinks[i].Name + "</a></li>"; 
		}
		$("#" + ulElmID).html(strMenuHtml);
	}
	function PopulateMenu()
	{
		PopulateLinks("menuBar");
	}