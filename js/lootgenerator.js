function GetRandomNumber(intNumMax)
{
	return Math.floor((Math.random() * intNumMax) + 1);
}
function GetIndexOfTableByName(strTableName)
{
	var indexOfTableName=-1;
	for(var i = 0; i < magicalTable.length; i++)
	{
		if(magicalTable[i].Name == strTableName)
		{
			indexOfTableName = i;
			break;
		}
	}
	return indexOfTableName;
}
function GetHtmlTable(strTableName, selectRandom)
{
	var indexOfTable = GetIndexOfTableByName(strTableName);
	if(indexOfTable == -1)
		return "<p style='color:red'>Error: Table Not Found.</p>";
	var itm = selectRandom ? (GetRandomItemFromTable(indexOfTable)) : null;
	//Temp Test Sub item
	//var itmName = "Figurine of wondrous power (roll d8)";
	//var subItmName = "Golden lions";
	var itmName = itm == null ? "" : itm.Item.Name;
	var subItmName = itm == null || itm.Subitem == null ? "" : itm.Subitem.Name;
	//console.log(itmName);
	//console.log(subItmName);
	var strHtml = "<div class='pre-scrollable table-responsive text-center'><table class='table text-center;'><tr><th>Item Name</th><th>Roll Range</th><th>Sub-items and Range</th></tr>";
	var minCurrentInt = 0;
	var maxCurrentInt = 0;
	for(var i = 0; i < magicalTable[indexOfTable].Items.length; i++)
	{
		minCurrentInt = maxCurrentInt + 1;
		maxCurrentInt = minCurrentInt + magicalTable[indexOfTable].Items[i].PercentageChance - 1;
		strHtml += "<tr" + (magicalTable[indexOfTable].Items[i].Name == itmName ? " class='current'" : "") + " ><td>" + magicalTable[indexOfTable].Items[i].Name + "</td><td>" + minCurrentInt + "-" + maxCurrentInt + "</td><td>";
		if(magicalTable[indexOfTable].Items[i].AdditionalChances)
		{
			strHtml += "<div class='table-responsive text-center'><table class='table text-center;'><tr><th>Subitem Name</th><th>Subitem Range</th></tr>";
			var minSubCurrentInt = 0;
			var maxSubCurrentInt = 0;
			for(var j = 0; j < magicalTable[indexOfTable].Items[i].AdditionalChances.length; j++)
			{
				minSubCurrentInt = maxSubCurrentInt + 1;
				maxSubCurrentInt = minSubCurrentInt + magicalTable[indexOfTable].Items[i].AdditionalChances[j].PercentageChance - 1;
				strHtml += "<tr" + (magicalTable[indexOfTable].Items[i].AdditionalChances[j].Name == subItmName ? " class='current'" : "") + " ><td>" + magicalTable[indexOfTable].Items[i].AdditionalChances[j].Name + "</td><td>" + minSubCurrentInt + "-" + maxSubCurrentInt + "</td></tr>";
			}
			strHtml += "</table></div>";
		}
		strHtml += "</td></tr>"; 
	}
	strHtml += "</table></div>";
	return strHtml;
}
function GetRandomItemFromTable(indexOfTableName)
{
	var randomInt = Math.floor((Math.random() * magicalTable[indexOfTableName].ChanceMax) + 1);
	//randomInt = 12;
	var randomSelection = null;
	var minCurrentInt = 0;
	var maxCurrentInt = 0;
	for(var i = 0; i < magicalTable[indexOfTableName].Items.length; i++)
	{
		minCurrentInt = maxCurrentInt + 1;
		maxCurrentInt = minCurrentInt + magicalTable[indexOfTableName].Items[i].PercentageChance - 1;
		if(randomInt >= minCurrentInt && randomInt <= maxCurrentInt)
		{
			randomSelection = magicalTable[indexOfTableName].Items[i];
			break;
		}
	}
	if(randomSelection == null)
	{
		alert("Table Not Found - No Random Selection");
			return null;
	}
	var additionalSelection = null;
	if(randomSelection.AdditionalChances)
	{
		minCurrentInt = 0;
		maxCurrentInt = 0;
		randomInt = Math.floor((Math.random() * randomSelection.ChanceMax) + 1);
		for(var i = 0; i < randomSelection.AdditionalChances.length; i++)
		{
			minCurrentInt = maxCurrentInt + 1;
			maxCurrentInt = minCurrentInt + randomSelection.AdditionalChances[i].PercentageChance - 1;
			if(randomInt >= minCurrentInt && randomInt <= maxCurrentInt)
			{
				additionalSelection = randomSelection.AdditionalChances[i];
				break;
			}
		}
		return {Item: randomSelection, Subitem:additionalSelection};
	}
	return { Item:randomSelection, Subitem:null};
}
function GetRandomItem(strTableName)
{
	var indexOfTableName = GetIndexOfTableByName(strTableName);
	if(indexOfTableName == -1)
	{
		return null;	
	}
	return GetRandomItemFromTable(indexOfTableName);
}

var magicalTable = [
	{
		Name: "Table A",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Potion of healing",
				PercentageChance:50
			},
			{
				Name: "Spell scroll (cantrip)",
				PercentageChance:10
			},
			{
				Name: "Potion of climbing",
				PercentageChance:10
			},
			{
				Name: "Spell scroll (1st level)",
				PercentageChance:20
			},
			{
				Name: "Spell scroll (2nd level)",
				PercentageChance:4
			},
			{
				Name: "Potion of greater healing",
				PercentageChance:4
			},
			{
				Name: "Bag of holding",
				PercentageChance:1
			},
			{
				Name: "Driftglobe",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table B",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Potion of greater healing",
				PercentageChance:15
			},
			{
				Name: "Potion of fire breathing",
				PercentageChance:7
			},
			{
				Name: "Potion of resistance",
				PercentageChance:7
			},
			{
				Name: "Ammunition,+1",
				PercentageChance:5
			},
			{
				Name: "Potion of animal friendship",
				PercentageChance:5
			},
			{
				Name: "Potion of hill giant strength",
				PercentageChance:5
			},
			{
				Name: "Potion of growth",
				PercentageChance:5
			},
			{
				Name: "Potion of water breathing",
				PercentageChance:5
			},
			{
				Name: "Spell scroll (2nd level)",
				PercentageChance:5
			},
			{
				Name: "Spell scroll (3rd level)",
				PercentageChance:5
			},
			{
				Name: "Bag of holding",
				PercentageChance:3
			},
			{
				Name: "Keoghtom's ointment",
				PercentageChance:3
			},
			{
				Name: "Oil of slipperiness",
				PercentageChance:3
			},
			{
				Name: "Dust of disappearance",
				PercentageChance:2
			},
			{
				Name: "Dust of dryness",
				PercentageChance:2
			},
			{
				Name: "Dust of sneezing and choking",
				PercentageChance:2
			},
			{
				Name: "Elemental gem",
				PercentageChance:2
			},
			{
				Name: "Philter of love",
				PercentageChance:2
			},
			{
				Name: "Alchemy jug",
				PercentageChance:1
			},
			{
				Name: "Cap of water breathing",
				PercentageChance:1
			},
			{
				Name: "Cloack of the manta ray",
				PercentageChance:1
			},
			{
				Name: "Driftglobe",
				PercentageChance:1
			},
			{
				Name: "Goggle of night",
				PercentageChance:1
			},
			{
				Name: "Helm of comprehending languages",
				PercentageChance:1
			},
			{
				Name: "Immovable rod",
				PercentageChance:1
			},
			{
				Name: "Lantern of revealing",
				PercentageChance:1
			},
			{
				Name: "Mariner's armor",
				PercentageChance:1
			},
			{
				Name: "Mithral armor",
				PercentageChance:1
			},
			{
				Name: "Potion of poison",
				PercentageChance:1
			},
			{
				Name: "Ring of swimming",
				PercentageChance:1
			},
			{
				Name: "Robe of useful items",
				PercentageChance:1
			},
			{
				Name: "Rope of climbing",
				PercentageChance:1
			},
			{
				Name: "Saddle of the cavalier",
				PercentageChance:1
			},
			{
				Name: "Wand of magic detection",
				PercentageChance:1
			},
			{
				Name: "Wand of secrets",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table C",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Potion of superior healing",
				PercentageChance:15
			},
			{
				Name: "Spell scroll (4th level)",
				PercentageChance:7
			},
			{
				Name: "Ammunition, +2",
				PercentageChance:5
			},
			{
				Name: "Potion of clairvoyance",
				PercentageChance:5
			},
			{
				Name: "Potion of diminution",
				PercentageChance:5
			},
			{
				Name: "Potion of gaseous form",
				PercentageChance:5
			},
			{
				Name: "Potion of frost giant strength",
				PercentageChance:5
			},
			{
				Name: "Potion of stone giant strength",
				PercentageChance:5
			},
			{
				Name: "Potion of heroism",
				PercentageChance:5
			},
			{
				Name: "Potion of invulnerability",
				PercentageChance:5
			},
			{
				Name: "Potion of mind reading",
				PercentageChance:5
			},
			{
				Name: "Spell scroll (5th level)",
				PercentageChance:5
			},
			{
				Name: "Elixir of health",
				PercentageChance:3
			},
			{
				Name: "Oil of etherealness",
				PercentageChance:3
			},
			{
				Name: "Potion of fire giant strength",
				PercentageChance:3
			},
			{
				Name: "Quaal's feather token",
				PercentageChance:3
			},
			{
				Name: "Scroll of protection",
				PercentageChance:3
			},
			{
				Name: "Bag of beans",
				PercentageChance:2
			},
			{
				Name: "Bead of force",
				PercentageChance:2
			},
			{
				Name: "Chime of opening",
				PercentageChance:1
			},
			{
				Name: "Decanter of endless water",
				PercentageChance:1
			},
			{
				Name: "Eyes of minute seeing",
				PercentageChance:1
			},
			{
				Name: "Folding boat",
				PercentageChance:1
			},
			{
				Name: "Heward's handy haversack",
				PercentageChance:1
			},
			{
				Name: "Horseshoes of speed",
				PercentageChance:1
			},
			{
				Name: "Necklace of fireballs",
				PercentageChance:1
			},
			{
				Name: "Periapt of health",
				PercentageChance:1
			},
			{
				Name: "Sending stones",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table D",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Potion of supreme healing",
				PercentageChance:20
			},
			{
				Name: "Potion of invisibility",
				PercentageChance:10
			},
			{
				Name: "Potion of speed",
				PercentageChance:10
			},
			{
				Name: "Spell scroll (6th level)",
				PercentageChance:10
			},
			{
				Name: "Spell scroll (7th level)",
				PercentageChance:7
			},
			{
				Name: "Ammunition, +3",
				PercentageChance:5
			},
			{
				Name: "Oil of sharpness",
				PercentageChance:5
			},
			{
				Name: "Potion of flying",
				PercentageChance:5
			},
			{
				Name: "Potion of cloud giant strength",
				PercentageChance:5
			},
			{
				Name: "Potion of longevity",
				PercentageChance:5
			},
			{
				Name: "Potion of vitality",
				PercentageChance:5
			},
			{
				Name: "Spell scroll (8th level)",
				PercentageChance:5
			},
			{
				Name: "Horseshoes of a zephyr",
				PercentageChance:3
			},
			{
				Name: "Nolzur's marvelous pigments",
				PercentageChance:3
			},
			{
				Name: "Bag of devouring",
				PercentageChance:1
			},
			{
				Name: "Portable hole",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table E",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Spell scroll (8th level)",
				PercentageChance:30
			},
			{
				Name: "Potion of storm giant strength",
				PercentageChance:25
			},
			{
				Name: "Potion of supreme healing",
				PercentageChance:15
			},
			{
				Name: "Spell scroll (9th level)",
				PercentageChance:15
			},
			{
				Name: "Universal solvent",
				PercentageChance:8
			},
			{
				Name: "Arrow of slaying",
				PercentageChance:5
			},
			{
				Name: "Sovereign glue",
				PercentageChance:2
			}
		]
	},
	{
		Name: "Table F",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Weapon, +1",
				PercentageChance:15
			},
			{
				Name: "Shield, +1",
				PercentageChance:3
			},
			{
				Name: "Sentinel shield",
				PercentageChance:3
			},
			{
				Name: "Amulet of proof against detection and location",
				PercentageChance:2
			},
			{
				Name: "Boots of elvenkind",
				PercentageChance:2
			},
			{
				Name: "Boots of striding and springing",
				PercentageChance:2
			},
			{
				Name: "Bracers of archery",
				PercentageChance:2
			},
			{
				Name: "Brooch of shielding",
				PercentageChance:2
			},
			{
				Name: "Broom of flying",
				PercentageChance:2
			},
			{
				Name: "Cloak of elvenkind",
				PercentageChance:2
			},
			{
				Name: "Cloak of protection",
				PercentageChance:2
			},
			{
				Name: "Gauntlets of ogre power",
				PercentageChance:2
			},
			{
				Name: "Hat of disguise",
				PercentageChance:2
			},
			{
				Name: "Javelin of lightning",
				PercentageChance:2
			},
			{
				Name: "Pearl of power",
				PercentageChance:2
			},
			{
				Name: "Rod of the pact keeper, +1",
				PercentageChance:2
			},
			{
				Name: "Slippers of spider climbing",
				PercentageChance:2
			},
			{
				Name: "Staff of the adder",
				PercentageChance:2
			},
			{
				Name: "Staff of the python",
				PercentageChance:2
			},
			{
				Name: "Sword of vengeance",
				PercentageChance:2
			},
			{
				Name: "Trident of fish command",
				PercentageChance:2
			},
			{
				Name: "Wand of magic missiles",
				PercentageChance:2
			},
			{
				Name: "Wand of the war mage, +1",
				PercentageChance:2
			},
			{
				Name: "Wand of web",
				PercentageChance:2
			},
			{
				Name: "Weapon of warning",
				PercentageChance:2
			},
			{
				Name: "Adamantine armor (chain mail)",
				PercentageChance:1
			},
			{
				Name: "Adamantine armor (chain shirt)",
				PercentageChance:1
			},
			{
				Name: "Adamantine armor (scale mail)",
				PercentageChance:1
			},
			{
				Name: "Bag of trick(gray)",
				PercentageChance:1
			},
			{
				Name: "Bag of trick(rust)",
				PercentageChance:1
			},
			{
				Name: "Bag of trick(tan)",
				PercentageChance:1
			},
			{
				Name: "Boots of winterlands",
				PercentageChance:1
			},
			{
				Name: "Circlet of blasting",
				PercentageChance:1
			},
			{
				Name: "Deck of illusions",
				PercentageChance:1
			},
			{
				Name: "Eversmoking bottle",
				PercentageChance:1
			},
			{
				Name: "Eyes of charming",
				PercentageChance:1
			},
			{
				Name: "Eyes of the eagle",
				PercentageChance:1
			},
			{
				Name: "Figurine of wondrous power (silver raven)",
				PercentageChance:1
			},
			{
				Name: "Gem of brightness",
				PercentageChance:1
			},
			{
				Name: "Gloves of missile snaring",
				PercentageChance:1
			},
			{
				Name: "Gloves of swimming and climbing",
				PercentageChance:1
			},
			{
				Name: "Gloves of thievery",
				PercentageChance:1
			},
			{
				Name: "Headband of intellect",
				PercentageChance:1
			},
			{
				Name: "Help of telepathy",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Doss lute)",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Fochlucan bandore)",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Mac-Fuimidh cittern)",
				PercentageChance:1
			},
			{
				Name: "Medallion of thoughts",
				PercentageChance:1
			},
			{
				Name: "Necklace of adaptation",
				PercentageChance:1
			},
			{
				Name: "Periapt of wound closure",
				PercentageChance:1
			},
			{
				Name: "Pipes of haunting",
				PercentageChance:1
			},
			{
				Name: "Pipes of the sewers",
				PercentageChance:1
			},
			{
				Name: "Ring of jumping",
				PercentageChance:1
			},
			{
				Name: "Ring of mind shielding",
				PercentageChance:1
			},
			{
				Name: "Ring of warmth",
				PercentageChance:1
			},
			{
				Name: "Ring of water walking",
				PercentageChance:1
			},
			{
				Name: "Quiver of Ehlonna",
				PercentageChance:1
			},
			{
				Name: "Stone of good luck",
				PercentageChance:1
			},
			{
				Name: "Wind fan",
				PercentageChance:1
			},
			{
				Name: "Winged boots",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table G",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Weapon, +2",
				PercentageChance:11
			},
			{
				Name: "Figurine of wondrous power (roll d8)",
				PercentageChance:3,
				ChanceMax:8,
				AdditionalChances:
				[
				   {
					 Name: "Bronze griffon",
					 PercentageChance:1
				   },
				   {
					 Name: "Ebony fly",
					 PercentageChance:1
				   },
				   {
					 Name: "Golden lions",
					 PercentageChance:1
				   },
				   {
					 Name: "Ivory goats",
					 PercentageChance:1
				   },
				   {
					 Name: "Marble elephant",
					 PercentageChance:1
				   },
				   {
					 Name: "Onyx dog",
					 PercentageChance:2
				   },
				   {
					 Name: "Sepentine owl",
					 PercentageChance:1
				   }
				]
			},
			{
				Name: "Adamantine armor (breastplate)",
				PercentageChance:1
			},
			{
				Name: "Adamantine armor (splint)",
				PercentageChance:1
			},
			{
				Name: "Amulet of health",
				PercentageChance:1
			},
			{
				Name: "Armor of vulnerability",
				PercentageChance:1
			},
			{
				Name: "Arrow-catching shield",
				PercentageChance:1
			},
			{
				Name: "Belt of dwarvenkind",
				PercentageChance:1
			},
			{
				Name: "Belt of hill giant strength",
				PercentageChance:1
			},
			{
				Name: "Beserker axe",
				PercentageChance:1
			},
			{
				Name: "Boot of levitation",
				PercentageChance:1
			},
			{
				Name: "Boots of speed",
				PercentageChance:1
			},
			{
				Name: "Bowl of commanding water elementals",
				PercentageChance:1
			},
			{
				Name: "Bracers of defense",
				PercentageChance:1
			},
			{
				Name: "Brazier of commanding fire elementals",
				PercentageChance:1
			},
			{
				Name: "Cape of the mountebank",
				PercentageChance:1
			},
			{
				Name: "Censer of controlling air elementals",
				PercentageChance:1
			},
			{
				Name: "Armor, +1 chain mail",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (chain mail)",
				PercentageChance:1
			},
			{
				Name: "Armor, +1 chain shirt",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (chain shirt)",
				PercentageChance:1
			},
			{
				Name: "Cloak of displacement",
				PercentageChance:1
			},
			{
				Name: "Cloak of the bat",
				PercentageChance:1
			},
			{
				Name: "Cube of force",
				PercentageChance:1
			},
			{
				Name: "Daern's instant fortress",
				PercentageChance:1
			},
			{
				Name: "Dagger of venom",
				PercentageChance:1
			},
			{
				Name: "Dimensional shackles",
				PercentageChance:1
			},
			{
				Name: "Dragon slayer",
				PercentageChance:1
			},
			{
				Name: "Elven chain",
				PercentageChance:1
			},
			{
				Name: "Flame tongue",
				PercentageChance:1
			},
			{
				Name: "Gem of seeing",
				PercentageChance:1
			},
			{
				Name: "Giant slayer",
				PercentageChance:1
			},
			{
				Name: "Glamoured studded leather",
				PercentageChance:1
			},
			{
				Name: "Helm of teleportation",
				PercentageChance:1
			},
			{
				Name: "Horn of blasting",
				PercentageChance:1
			},
			{
				Name: "Horn of Valhalla (silver or brass)",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Canaith mandolin)",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Cli lyre)",
				PercentageChance:1
			},
			{
				Name: "loun stone (awareness)",
				PercentageChance:1
			},
			{
				Name: "loun stone (protection)",
				PercentageChance:1
			},
			{
				Name: "loun stone (reserve)",
				PercentageChance:1
			},
			{
				Name: "loun stone (sustenance)",
				PercentageChance:1
			},
			{
				Name: "Iron bands of Bilarro",
				PercentageChance:1
			},
			{
				Name: "Armor, +1 leather",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (leather)",
				PercentageChance:1
			},
			{
				Name: "Mace of disruption",
				PercentageChance:1
			},
			{
				Name: "Mace of smiting",
				PercentageChance:1
			},
			{
				Name: "Mace of terror",
				PercentageChance:1
			},
			{
				Name: "Mantle of spell resistance",
				PercentageChance:1
			},
			{
				Name: "Necklace of prayer beads",
				PercentageChance:1
			},
			{
				Name: "Periapt of proof against poison",
				PercentageChance:1
			},
			{
				Name: "Ring of animal influence",
				PercentageChance:1
			},
			{
				Name: "Ring of evasion",
				PercentageChance:1
			},
			{
				Name: "Ring of feather falling",
				PercentageChance:1
			},
			{
				Name: "Ring of free action",
				PercentageChance:1
			},
			{
				Name: "Ring of protection",
				PercentageChance:1
			},
			{
				Name: "Ring of resistance",
				PercentageChance:1
			},
			{
				Name: "Ring of spell storing",
				PercentageChance:1
			},
			{
				Name: "Ring of the ram",
				PercentageChance:1
			},
			{
				Name: "Ring of X-ray vision",
				PercentageChance:1
			},
			{
				Name: "Robe of eyes",
				PercentageChance:1
			},
			{
				Name: "Rod of rulership",
				PercentageChance:1
			},
			{
				Name: "Rod of the pact keeper, +2",
				PercentageChance:1
			},
			{
				Name: "Rope of entanglement",
				PercentageChance:1
			},
			{
				Name: "Armor, +1 scale mail",
				PercentageChance:1
			},
			{
				Name: "Armor of resitance (scale mail)",
				PercentageChance:1
			},
			{
				Name: "Shield, +2",
				PercentageChance:1
			},
			{
				Name: "Shield of missile attraction",
				PercentageChance:1
			},
			{
				Name: "Staff of charming",
				PercentageChance:1
			},
			{
				Name: "Staff of healing",
				PercentageChance:1
			},
			{
				Name: "Staff of swarming insects",
				PercentageChance:1
			},
			{
				Name: "Staff of the woodlands",
				PercentageChance:1
			},
			{
				Name: "Staff of withering",
				PercentageChance:1
			},
			{
				Name: "Stone of controlling earth elementals",
				PercentageChance:1
			},
			{
				Name: "Sun blade",
				PercentageChance:1
			},
			{
				Name: "Sword of life stealing",
				PercentageChance:1
			},
			{
				Name: "Sword of wounding",
				PercentageChance:1
			},
			{
				Name: "Tentacle rod",
				PercentageChance:1
			},
			{
				Name: "Vicious weapon",
				PercentageChance:1
			},
			{
				Name: "Wand of binding",
				PercentageChance:1
			},
			{
				Name: "Wand of enemy detection",
				PercentageChance:1
			},
			{
				Name: "Wand of fear",
				PercentageChance:1
			},
			{
				Name: "Wand of fireballs",
				PercentageChance:1
			},
			{
				Name: "Wand of lightning bolts",
				PercentageChance:1
			},
			{
				Name: "Wand of paralysis",
				PercentageChance:1
			},
			{
				Name: "Wand of the war mage, +2",
				PercentageChance:1
			},
			{
				Name: "Wand of wonder",
				PercentageChance:1
			},
			{
				Name: "Wings of flying",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table H",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Weapon, +3",
				PercentageChance:10
			},
			{
				Name: "Amulet of the planes",
				PercentageChance:2
			},
			{
				Name: "Carpet of flying",
				PercentageChance:2
			},
			{
				Name: "Crystal ball (very rare version)",
				PercentageChance:2
			},
			{
				Name: "Ring of regeneration",
				PercentageChance:2
			},
			{
				Name: "Ring of shooting stars",
				PercentageChance:2
			},
			{
				Name: "Ring of telekinesis",
				PercentageChance:2
			},
			{
				Name: "Robe of scintillating colors",
				PercentageChance:2
			},
			{
				Name: "Robe of stars",
				PercentageChance:2
			},
			{
				Name: "Rod of absorption",
				PercentageChance:2
			},
			{
				Name: "Rod of alertness",
				PercentageChance:2
			},
			{
				Name: "Rod of security",
				PercentageChance:2
			},
			{
				Name: "Rod of the pact keeper,+3",
				PercentageChance:2
			},
			{
				Name: "Scimitar of speed",
				PercentageChance:2
			},
			{
				Name: "Shield,+3",
				PercentageChance:2
			},
			{
				Name: "Staff of fire",
				PercentageChance:2
			},
			{
				Name: "Staff of frost",
				PercentageChance:2
			},
			{
				Name: "Staff of power",
				PercentageChance:2
			},
			{
				Name: "Staff of striking",
				PercentageChance:2
			},
			{
				Name: "Staff of thunder and lightning",
				PercentageChance:2
			},
			{
				Name: "Sword of sharpness",
				PercentageChance:2
			},
			{
				Name: "Wand of polymorph",
				PercentageChance:2
			},
			{
				Name: "Wand of the war mage,+3",
				PercentageChance:2
			},
			{
				Name: "Adamantine armor (half plate)",
				PercentageChance:1
			},
			{
				Name: "Adamantine armor (plate)",
				PercentageChance:1
			},
			{
				Name: "Animated Shield",
				PercentageChance:1
			},
			{
				Name: "Belt of fire giant strength",
				PercentageChance:1
			},
			{
				Name: "Belt of frost (or stone) giant strength",
				PercentageChance:1
			},
			{
				Name: "Armor,+1 breastplate",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (breastplate)",
				PercentageChance:1
			},
			{
				Name: "Candle of invocation",
				PercentageChance:1
			},
			{
				Name: "Armor,+2 chain mail",
				PercentageChance:1
			},
			{
				Name: "Armor,+2 chain shirt",
				PercentageChance:1
			},
			{
				Name: "Cloak of arachnida",
				PercentageChance:1
			},
			{
				Name: "Dancing sword",
				PercentageChance:1
			},
			{
				Name: "Demon armor",
				PercentageChance:1
			},
			{
				Name: "Dragon scale mail",
				PercentageChance:1
			},
			{
				Name: "Dwarven plate",
				PercentageChance:1
			},
			{
				Name: "Dwarven thrower",
				PercentageChance:1
			},
			{
				Name: "Efreeti bottle",
				PercentageChance:1
			},
			{
				Name: "Figurine of wondrous power (obsidian steed)",
				PercentageChance:1
			},
			{
				Name: "Frost brand",
				PercentageChance:1
			},
			{
				Name: "Helm of brilliance",
				PercentageChance:1
			},
			{
				Name: "Horn of Valhalla (bronze)",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Anstruth harp)",
				PercentageChance:1
			},
			{
				Name: "loun stone (absorption)",
				PercentageChance:1
			},
			{
				Name: "loun stone (agility)",
				PercentageChance:1
			},
			{
				Name: "loun stone (fortitude)",
				PercentageChance:1
			},
			{
				Name: "loun stone (insight)",
				PercentageChance:1
			},
			{
				Name: "loun stone (intellect)",
				PercentageChance:1
			},
			{
				Name: "loun stone (leadership)",
				PercentageChance:1
			},
			{
				Name: "loun stone (strength)",
				PercentageChance:1
			},
			{
				Name: "Armor,+2 leather",
				PercentageChance:1
			},
			{
				Name: "Manual of bodily health",
				PercentageChance:1
			},
			{
				Name: "Manual of gainful exercise",
				PercentageChance:1
			},
			{
				Name: "Manual of golems",
				PercentageChance:1
			},
			{
				Name: "Manual of quickness of action",
				PercentageChance:1
			},
			{
				Name: "Mirror of life trapping",
				PercentageChance:1
			},
			{
				Name: "Nine lives stealer",
				PercentageChance:1
			},
			{
				Name: "Oathbow",
				PercentageChance:1
			},
			{
				Name: "Armor,+2 scale mail",
				PercentageChance:1
			},
			{
				Name: "Spellguard shield",
				PercentageChance:1
			},
			{
				Name: "Armor,+1 splint",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (splint)",
				PercentageChance:1
			},
			{
				Name: "Armor,+1 studded leather",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (studded leather)",
				PercentageChance:1
			},
			{
				Name: "Tome of clear though",
				PercentageChance:1
			},
			{
				Name: "Tome of leadership and understanding",
				PercentageChance:1
			},
			{
				Name: "Tome of understanding",
				PercentageChance:1
			}
		]
	},
	{
		Name: "Table I",
		ChanceMax:100,
		Items:
		[
			{
				Name: "Defender",
				PercentageChance:5
			},
			{
				Name: "Hammer of thunderbolts",
				PercentageChance:5
				
			},
			{
				Name: "Luck blade",
				PercentageChance:5
			},
			{
				Name: "Sword of answering",
				PercentageChance:5
			},
			{
				Name: "Holy avenger",
				PercentageChance:3
			},
			{
				Name: "Ring of djinni summoning",
				PercentageChance:3
			},
			{
				Name: "Ring of invisibility",
				PercentageChance:3
			},
			{
				Name: "Ring of spell turning",
				PercentageChance:3
			},
			{
				Name: "Rod of lordly might",
				PercentageChance:3
			},
			{
				Name: "Staff of the magi",
				PercentageChance:3
			},
			{
				Name: "Vorpal sword",
				PercentageChance:3
			},
			{
				Name: "Belt of cloud giant strength",
				PercentageChance:2
			},
			{
				Name: "Armor,+2 breastplate",
				PercentageChance:2
			},
			{
				Name: "Armor,+3 chain mail",
				PercentageChance:2
			},
			{
				Name: "Armor,+3 chain shirt",
				PercentageChance:2
			},
			{
				Name: "Cloak of invisibility",
				PercentageChance:2
			},
			{
				Name: "Crystal ball (legendary version)",
				PercentageChance:2
			},
			{
				Name: "Armor,+1 half plate",
				PercentageChance:2
			},
			{
				Name: "Iron flask",
				PercentageChance:2
			},
			{
				Name: "Armor,+3 leather",
				PercentageChance:2
			},
			{
				Name: "Armor,+1 plate",
				PercentageChance:2
			},
			{
				Name: "Robe of the archmagi",
				PercentageChance:2
			},
			{
				Name: "Rod of resurrection",
				PercentageChance:2
			},
			{
				Name: "Armor,+1 scale mail",
				PercentageChance:2
			},
			{
				Name: "Scarab of protection",
				PercentageChance:2
			},
			{
				Name: "Armor,+2 splint",
				PercentageChance:2
			},
			{
				Name: "Armor,+2 studded leather",
				PercentageChance:2
			},
			{
				Name: "Well of many worlds",
				PercentageChance:2
			},
			{
				Name: "Magic armor (roll d12)",
				PercentageChance:1,
				ChanceMax:12,
				AdditionalChances:
				[
					{
					  Name:"Armor,+2 plate",
					  PercentageChance:2
					},
					{
					  Name:"Armor,+2 plate",
					  PercentageChance:2
					},
					{
					  Name:"Armor,+3 studded leather",
					  PercentageChance:2
					},
					{
					  Name:"Armor,+3 breastplate",
					  PercentageChance:2
					},
					{
					  Name:"Armor,+3 splint",
					  PercentageChance:2
					},
					{
					  Name:"Armor,+3 half plate",
					  PercentageChance:1
					},
					{
					  Name:"Armor,+3 plate",
					  PercentageChance:1
					}
				]
			},
			{
				Name: "Apparatus of Kwalish",
				PercentageChance:1
			},
			{
				Name: "Armor of invulnerability",
				PercentageChance:1
			},
			{
				Name: "Belt of storm giant strength",
				PercentageChance:1
			},
			{
				Name: "Cubic gate",
				PercentageChance:1
			},
			{
				Name: "Deck of many things",
				PercentageChance:1
			},
			{
				Name: "Efreeti chain",
				PercentageChance:1
			},
			{
				Name: "Armor of resistance (half plate)",
				PercentageChance:1
			},
			{
				Name: "Horn of Valhalla (iron)",
				PercentageChance:1
			},
			{
				Name: "Instrument of the bards (Ollamh harp)",
				PercentageChance:1
			},
			{
				Name: "loun stone (greater absorption)",
				PercentageChance:1
			},
			{
				Name: "loun stone (mastery)",
				PercentageChance:1
			},
			{
				Name: "loun stone (regeneration)",
				PercentageChance:1
			},
			{
				Name: "Plate armor of etherealness",
				PercentageChance:1
			},
			{
				Name: "Plate armor of resistance",
				PercentageChance:1
			},
			{
				Name: "Ring of air elemental command",
				PercentageChance:1
			},
			{
				Name: "Ring of earth elemental command",
				PercentageChance:1
			},
			{
				Name: "Ring of fire elemental command",
				PercentageChance:1
			},
			{
				Name: "Ring of three wishes",
				PercentageChance:1
			},
			{
				Name: "Ring of water elemental command",
				PercentageChance:1
			},
			{
				Name: "Sphere of annihilation",
				PercentageChance:1
			},
			{
				Name: "Talisman of pure good",
				PercentageChance:1
			},
			{
				Name: "Talisman of the sphere",
				PercentageChance:1
			},
			{
				Name: "Talisman of ultimate evil",
				PercentageChance:1
			},
			{
				Name: "Tome of the stilled tongue",
				PercentageChance:1
			}
		]
	}
]