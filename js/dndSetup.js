		function PlayerAdd()
	{
		AddPlayerFromForm();
		DisplayDMArray(POPULATEID);
	}
	function DisplayNextTurn()
	{
		NextCharacterTurn();
		DisplayDMArray(POPULATEID);
	}
	function DisplayPreviousTurn()
	{
		PreviousCharacterTurn();
		DisplayDMArray(POPULATEID);
	}
	function RemoveAfflictionAndDisplay(strID, strAffliction)
	{
		RemoveAffliction(strID, strAffliction);
		DisplayDMArray(POPULATEID);
		ScrollToElement("row" + strID, false);
	}
	function ToggleTurnHoldAndDisplay(intID)
	{
		ToggleTurnHold(intID);
		DisplayDMArray(POPULATEID);
		ScrollToElement("row" + intID, false);
	}
	function ToggleHiddenAndDisplay(intID)
	{
		ToggleHidden(intID);
		DisplayDMArray(POPULATEID);
		ScrollToElement("row" + intID, false);
	}
	function ToggleActiveAndDisplay(intID)
	{
		ToggleActive(intID);
		DisplayDMArray(POPULATEID);
		ScrollToElement("row" + intID, false);
	}
	function ClearCharacters()
	{
		RemoveAllCharacters();
		DisplayDMArray(POPULATEID);
	}
	function RemoveCharacterAndDisplay(intID)
	{
		RemoveCharacter(intID);
		DisplayDMArray(POPULATEID);
	}
	function ChangeDDL()
	{
		if($("#ddlCharacter option:selected").text() == "Enemy")
		{
		    $("#tokenDiv").show();
			$("#txtToken").val("");
		}
		else
		{
			$("#tokenDiv").hide();
			$("#txtToken").val("-1");
		}
	}
	function AddAfflictionAndDisplay(strID)
	{
		AddAffliction(strID);
		DisplayDMArray(POPULATEID);
	}
	function MoveCharacterAndDisplay(strID, isBelow)
	{
		MoveCharacter(strID, isBelow);
		DisplayDMArray(POPULATEID);
	}
	function AddPlayerFromForm()
	{
		var txtDDLVal = $("#ddlCharacter option:selected").text();
		var isNPC = txtDDLVal == "NPC";
		var isEnemy = txtDDLVal == "Enemy";
		var isHidden = $("#cbHidden").is(':checked');
		var isActive = $("#cbActive").is(':checked');
		var tokenNumber = Number($("#txtToken").val());
		AddEntity($('#txtName').val().trim(), Number($('#txtCurrentHP').val().trim()), Number($('#txtMaxHP').val().trim()), isNPC, isEnemy, isHidden, isActive, tokenNumber);
		if(isEnemy && tokenNumber > 0)
		{
			$("#txtToken").val((tokenNumber + 1));
			AddToXP($("#txtXP").val().trim());
		}
	}
	function UpdateNotificationFromCheckbox()
	{
		ToggleViewerNotifications($("#cbNotification").is(':checked'));
	}
	function UpdateCheckbox(elmID, boolVal)
	{
		$('#' + elmID).attr('checked', boolVal);
	}
	function UpdateNotificationCheckboxFromStorage()
	{
		UpdateCheckbox("cbNotification", GetStorageItem(lsNotifyKey) == "true");
	}
	function AddToXP(AddXPNum)
	{
		var convertedXP = Number(AddXPNum);
		if(isNaN(AddXPNum) || convertedXP <= 0)
		{
			ShowNotification("warning", "Warning: Invalid XP - XP Add Ignored.");
			return;
		}
		var currentXP = GetXP();
		currentXP = currentXP + convertedXP;
		SetXP(currentXP);
	}
	function GetXP()
	{
		var strCurrentXP = GetStorageItem("XP");
		var currentXP = 0;
		if(strCurrentXP)
			currentXP = Number(strCurrentXP);
		return currentXP;
	}
	function SetXP(XPNum)
	{
		SetStorageItem("XP", XPNum);
	}
	function ClearXP()
	{
		SetXP(0);
	}
	function ResetRound()
	{
		SetRound(1);
	}
	function SetRound(strRoundNum)
	{
		var intNum = Number(strRoundNum);
		if(isNaN(strRoundNum) || intNum <= 0)
		{
			ShowNotification("error", "Error: Invalid Round number.");
			return;
		}
		SetStorageItem("Round", strRoundNum)
	}
	function IncrementRound()
	{
		var intRound = GetRound();
		intRound = intRound + 1;
		SetRound(intRound);
	}
	function DecrementRound()
	{
		var intRound = GetRound();
		if(intRound > 1)
		{
			intRound = intRound - 1;			
		}
		SetRound(intRound);
	}
	function ToggleViewerNotifications(enable)
	{
		SetStorageItem(lsNotifyKey, (enable ? "true" : "false"));
		AddMessageToQueue("notification", "Messages have now been " + (enable ? "enabled - viewers will now see notifications." : "disabled - viewers will no longer see notifications."), -1, true);
	}
	function MoveCharacter(intID, isBelow)
	{
		var characters = GetCharacters();
		var currentCharIndex = GetIndexOfCharacterByIDPassArray(intID, characters);
		var toIndex = -1;
		if(currentCharIndex == -1)
			return false;
		if(isBelow)
		{
			if(currentCharIndex == (characters.length - 1))
				toIndex = 0;
			else
			{
				toIndex = currentCharIndex + 1;
			}
		}
		else //Is Above
		{
			if(currentCharIndex == 0)
				toIndex = characters.length - 1;
			else
			{
				toIndex = currentCharIndex - 1;
			}
		}
		characters.moveByCharacterID(intID, characters[toIndex].ID);
		SetCharacters(characters);
	}
		function ToggleTurnHold(intID)
	{
		var characters = GetCharacters();
		var indexOfCharacter = GetIndexOfCharacterByIDPassArray(intID, characters);
		if(indexOfCharacter == -1)
		{
			ShowNotification("error", "Error: Character does not exist!");
			return;
		}
		characters[indexOfCharacter].TurnHeld = !characters[indexOfCharacter].TurnHeld;
		SetCharacters(characters);
	}
	function ToggleHidden(intID)
	{
		var characters = GetCharacters();
		var indexOfCharacter = GetIndexOfCharacterByIDPassArray(intID, characters);
		if(indexOfCharacter == -1)
		{
			ShowNotification("error", "Error: Character does not exist!");
			return;
		}
		characters[indexOfCharacter].IsHidden = !characters[indexOfCharacter].IsHidden;
		SetCharacters(characters);
	}
	function ToggleActive(intID)
	{
		var characters = GetCharacters();
		var indexOfCharacter = GetIndexOfCharacterByIDPassArray(intID, characters);
		if(indexOfCharacter == -1)
		{
			ShowNotification("error", "Error: Character does not exist!");
			return;
		}
		characters[indexOfCharacter].IsActive = characters[indexOfCharacter].IsActive == true ? false : true;
		SetCharacters(characters);
	}
	function UpdateMaxHitPointsAndDisplay(strID)
	{
		var strHPMax = Number($("#c" + strID + "_MAXHP").val());
		if(strHPMax <= 0)
		{
			ShowNotification("error", "Error: Invalid Max HP entered!");
			return;
		}
		var characters = GetCharacters();
		var indexOfCharacter = GetIndexOfCharacterByIDPassArray(strID, characters);
		if(indexOfCharacter == -1)
		{
			ShowNotification("error", "Error: Character does not exist!");
			return;
		}
		var isIncrease = characters[indexOfCharacter].MaxHP < strHPMax;
		if(characters[indexOfCharacter].CurrentHP > strHPMax)
			characters[indexOfCharacter].CurrentHP = strHPMax;
		characters[indexOfCharacter].MaxHP = strHPMax;
		SetCharacters(characters);
		DisplayDMArray(POPULATEID);
		AddMessageToQueue(isIncrease ? "success" : "error", GetAllowedName(characters[indexOfCharacter]) +  " has had their maximum hit points " + (isIncrease ? "increased" : "decreased") + "!", characters[indexOfCharacter].ID);
	}
	function HitPointModify(strID, isIncrease)
	{
		ModifyCharacterHitPoints(strID, isIncrease);
		DisplayDMArray(POPULATEID);
	}
	function ModifyCharacterHitPoints(strID, isIncrease)
	{
		var intHP = Number($("#c" + strID + "_HP").val());
		if(intHP <= 0)
		{
			ShowNotification("error", "Error: Invalid HP entered!");
			return;
		}
		var characters = GetCharacters();
		var indexOfCharacter = GetIndexOfCharacterByIDPassArray(strID, characters);
		var died = false;
		if(indexOfCharacter == -1)
		{
			ShowNotification("error", "Error: Character does not exist!");
			return;
		}
		var type = (isIncrease && !characters[indexOfCharacter].IsEnemy) || (!isIncrease && characters[indexOfCharacter].IsEnemy) ? "success" : "error";
		if(isIncrease)
		{
			
			if(characters[indexOfCharacter].CurrentHP == 0 && intHP > 0 && characters[indexOfCharacter].Afflictions != null)
			{
				var indexFound = -1;
				for(var j = 0; j < characters[indexOfCharacter].Afflictions.length; j++)
				{
					if(characters[indexOfCharacter].Afflictions[j].Name == UnconsciousAffliction.Name)
					{
						indexFound = j;
						break;
					}
				}
				if(indexFound != -1)
					characters[indexOfCharacter].Afflictions.splice(indexFound, 1);
			}
			characters[indexOfCharacter].CurrentHP += intHP;
			if(characters[indexOfCharacter].CurrentHP > characters[indexOfCharacter].MaxHP)
				characters[indexOfCharacter].CurrentHP = characters[indexOfCharacter].MaxHP;				
		}
		else
		{
			characters[indexOfCharacter].CurrentHP -= intHP;
			if(characters[indexOfCharacter].CurrentHP <= 0)
			{
				characters[indexOfCharacter].CurrentHP = 0;
				died = true;
				if(characters[indexOfCharacter].Afflictions == null)
					characters[indexOfCharacter].Afflictions = [];
				characters[indexOfCharacter].Afflictions.push(UnconsciousAffliction);
			}
		}
		if(died)
		{
			if(RemoveCharacter(strID))
				died = true;
			else
				died = false;
		}
		if(!died)
		{
			SetCharacters(characters);
			AddMessageToQueue(type, GetAllowedName(characters[indexOfCharacter]) +  " has " + (isIncrease ? "regained" : "lost") + " hit points!", characters[indexOfCharacter].ID);
		}
	}
	function AlignTables()
	{
		for(var i = 1; i <= 9; i++)
		{
			theWidth = $("#dmtable tr > td:nth-child(" + i + ")").width();
			$("#headTable th:nth-child(" + i + ")").width(theWidth + "px");	
		}
		$("#dmtable th").hide();
		for(var i = 1; i <= 9; i++)
		{
			theWidth = $("#headTable th:nth-child(" + i + ")").width();
			$("#dmtable tr > td:nth-child(" + i + ")").width(theWidth + "px");	
		}
		$("#headTable").css("margin-bottom", "0");
	}
		function DisplayDMArray()
	{
		var stringBuilder = "<div class='table-responsive text-center'><table id='dmtable' class='table text-center;' ><thead><tr><th>Name</th><th>Hit Points</th><th>+/- HP</th><th>Order</th><th>+ Affliction</th><th>- Affliction</th><th>Hidden</th><th>Active</th><th>Turn Held</th></tr></thead><tbody>";
		var availChar = GetCharacters();
		var strSelect = GetAfflictionSelectOptions();
		var strReplaced;
		var regexExp = new RegExp(CHARNAMEPLACEHOLDER, 'g');
		$("#" + POPULATEID).show();
		for(var i = 0; i < availChar.length; i++)
		{
			stringBuilder += "<tr" + (availChar[i].CurrentTurn ? " class='current'" : "")  + " id='row" + availChar[i].ID + "' ><td>" + availChar[i].Name  + (availChar[i].TokenNumber <= 0 || availChar[i].TokenNumber == null ? "" : "{" + availChar[i].TokenNumber + "}") + ( availChar[i].IsEnemy ? "[E]" : "") + "</td>";
			stringBuilder += "<td>" + availChar[i].CurrentHP + "/<input style='width:75px;' type='number' id='c" + availChar[i].ID + "_MAXHP' value='" + availChar[i].MaxHP + "'><br><button type='button' class='btn btn-default' onclick='UpdateMaxHitPointsAndDisplay(" + availChar[i].ID + ");'>Update Max HP</button></td>";
			stringBuilder += "<td><input id='c" + availChar[i].ID + "_HP' style='width:75px;'  value='0' type='number'><br><button type='button' onclick='HitPointModify(" + availChar[i].ID + ", true);'><img src='img/plus.png' width='25' height='25'></button><button type='button' onclick='HitPointModify(" + availChar[i].ID + ", false);'><img src='img/minus.png' width='25' height='25'></button></td>";
			stringBuilder += "<td><img src='img/uparrow.png' onclick='MoveCharacterAndDisplay("  + availChar[i].ID + ", false);' width='30' height='30'><br><img src='img/downarrow.png' width='30' height='30' onclick='MoveCharacterAndDisplay(" + availChar[i].ID +", true);'></td>";
			stringBuilder += "<td>" +  strSelect.replace(regexExp, availChar[i].ID); + "</td>";
			stringBuilder += "<td>";
			for(var j = 0; j < availChar[i].Afflictions.length; j++)
				stringBuilder += "<span data-toggle='tooltipHTML' class='bsTooltip' data-placement='bottom' data-original-title='" + availChar[i].Afflictions[j].Tooltip + "'>" + availChar[i].Afflictions[j].Name + "</span><button class='btn btn-default' onclick='RemoveAfflictionAndDisplay(" + availChar[i].ID + ",\"" + availChar[i].Afflictions[j].Name + "\");'>[X]</button><br>" ;	
			stringBuilder+= "</td>";
			stringBuilder += "<td><button onclick='ToggleHiddenAndDisplay(" + availChar[i].ID + ");' class='btn btn-default'>" + (availChar[i].IsHidden ? "Yes" : "No") + "</button></td>";
			stringBuilder += "<td><button onclick='ToggleActiveAndDisplay(" + availChar[i].ID + ");' class='btn btn-default'>" + (availChar[i].IsActive ? "Yes" : "No") + "</button></td>";
			stringBuilder += "<td><button onclick='ToggleTurnHoldAndDisplay(" + availChar[i].ID + ");' class='btn btn-default'>" + (availChar[i].TurnHeld ? "Yes" : "No") + "</button></td>";
			stringBuilder+= "</tr>";
		}
		stringBuilder += "</tbody></table></div>";
		var theWidth;
		$("#" + POPULATEID).html(stringBuilder);
		if(availChar.length == 0)
			$("#" + POPULATEID).hide();
		AlignTables();
		ResetTooltip();
		SetXPElement();
		SetRoundElement();
		return false;
	}
	function AddAffliction(strID)
	{
		var affID = "ddlAffliction" + strID;
		var selectedAffliction = $("#" + affID + " option:selected").text();
		return AddPlayerAffliction(strID, selectedAffliction);
	}
	function RemoveAffliction(strID, strAffliction)
	{
		var characters = GetCharacters();
		var characterIndex = GetIndexOfCharacterByIDPassArray(strID, characters);
		if(characterIndex == -1)
			return false;
		if(characters[characterIndex].Afflictions == null)
			return false;
		var indexFound = -1;
		for(var i = 0; i < characters[characterIndex].Afflictions.length; i++)
		{
			if(characters[characterIndex].Afflictions[i].Name == strAffliction)
			{	
				indexFound = i;
				break;
			}
		}
		if(indexFound == -1)
			return false;
		characters[characterIndex].Afflictions.splice(indexFound, 1);
		SetCharacters(characters);
		return true;
	}
	function AddPlayerAffliction(strID, selectedAffliction)
	{
		var characters = GetCharacters();
		var characterIndex = GetIndexOfCharacterByIDPassArray(strID, characters);
		if(characterIndex == -1)
		{
			ShowNotification("error", "Error Adding Affliction: Character not found!");
			return false;	
		}
		var result = Afflictions.filter(function( obj ) {
		  return obj.Name == selectedAffliction;
		});
		if(result.length == 0)
		{
			ShowNotification("error", "Error Adding Affliction: Affliction " + selectedAffliction + " not found!");
			return false;	
		}
		if(characters[characterIndex].Afflictions == null)
			characters[characterIndex].Afflictions = [];
		else
		{
			var afflictionInChar = characters[characterIndex].Afflictions.filter(function( obj ) {
				return obj.Name == selectedAffliction;
			});
			if(afflictionInChar.length > 0)
			{
				ShowNotification("error", characters[characterIndex].Name + " already afflicted by " + selectedAffliction);
				return false;
			}
		}
		var affliction = result[0];
		characters[characterIndex].Afflictions.push(affliction);
		SetCharacters(characters);
		var type = (characters[characterIndex].IsEnemy && result[0].Bad) || (!characters[characterIndex].IsEnemy && !result[0].Bad) ? "success" : "error";
		AddMessageToQueue(type, GetAllowedName(characters[characterIndex]) + " is now " + (affliction.By ? "afflicted by " : "") + affliction.Name + ".", -1);
		return true;
	}
	function GetAfflictionSelectOptions()
	{
		var strSelect = '<select id="ddlAffliction' + CHARNAMEPLACEHOLDER + '" class="input" >';
		for(var i = 0; i < Afflictions.length; i++)
		{
			strSelect += "<option>" + Afflictions[i].Name + "</option>";
		}
		strSelect += "</select><img src='img/plus.png' onclick='AddAfflictionAndDisplay(\""  + CHARNAMEPLACEHOLDER + "\");' width='30' height='30'>";
		return strSelect;
	}
	function AddMessageToQueue(strType, strMessage, intID, systemMessage)
	{
		if(systemMessage === null)
			systemMessage = false;
		var messages = GetMessages();
		var nextMsg = {"Type": strType, "Message": strMessage, "EntityID": intID, "IsSystemMessage": systemMessage ? true : false};
		messages.push(nextMsg);
		SetMessages(messages);
		ShowNotification("notification", strMessage, false);
	}
	function RemoveAllCharacters()
	{
		NotyConfirm("Are you sure you want to delete all characters?", function() {ClearLocalStorageItem(CHARITEM); DisplayDMArray(POPULATEID);}, function(){});
	}
	//Remove Functions
	function RemoveCharacter(strID)
	{
		var ID = Number(strID);
		var removeCharacter = GetCharacterByID(strID);
		var type = removeCharacter.IsEnemy ? "success" : "error";
		if(removeCharacter == null)
		{
			ShowNotification("error", "Cannot remove character, character doesn't exist!");
			return false;
		}
		NotyConfirm("Do you wish to permanently delete the character " + removeCharacter.Name + "?", 
		function() 
		{
			var characters = GetCharacters();
			var indexOfCharacter = characters.indexOf(removeCharacter);
			for (var i = characters.length - 1; i >= 0; --i) {
				if (characters[i].ID == ID) {
					characters.splice(i,1);
				}
			}
			SetCharacters(characters);
			DisplayDMArray(POPULATEID);
			AddMessageToQueue(type, GetAllowedName(removeCharacter) + " has been slain!", -1);
			confirmVal = true;
		},
		function()
		{
			confirmVal = false;
		}
		);
		return confirmVal;
	}
	
	//Modify Functions
	 Array.prototype.move = function(pos1, pos2) {
    // local variables
    var i, tmp;
    // cast input parameters to integers
    pos1 = parseInt(pos1, 10);
    pos2 = parseInt(pos2, 10);
    // if positions are different and inside array
    if (pos1 !== pos2 && 0 <= pos1 && pos1 <= this.length && 0 <= pos2 && pos2 <= this.length) {
      // save element from position 1
      tmp = this[pos1];
      // move element down and shift other elements up
      if (pos1 < pos2) {
        for (i = pos1; i < pos2; i++) {
          this[i] = this[i + 1];
        }
      }
      // move element up and shift other elements down
      else {
        for (i = pos1; i > pos2; i--) {
          this[i] = this[i - 1];
        }
      }
      // put element from position 1 to destination
      this[pos2] = tmp;
    }
  }
  Array.prototype.moveByCharacterID = function(IDFrom, IDTo) {
	  var indexes = GetIndexesByCharacterIDPassArray(IDFrom, IDTo, this);
	  if(indexes[0] == -1 || indexes[1] == -1)
		  return;
	  var adjustedTo = indexes[1];
	  this.move(indexes[0], adjustedTo);
	  
  }
 	function ToggleCharacterActivity(strName)
	{
		var character = GetCharacterByName(strName);
	}
	function PreviousCharacterTurn()
	{
		CharacterTurn(false);
	}
	function NextCharacterTurn()
	{
		CharacterTurn(true);
	}
	//Add Functions
	function AddEntity(strName, intCurHP, intMaxHP, isNPC, isEnemy, isHidden, isActive, tokenNumber)
	{
		var strType = isEnemy ? "enemy" : "character" ;
		if(!strName)
		{
			ShowNotification("error", "Error: No Name for character entered!");
			return;
		}
	    if(!isEnemy && CharacterExists(strName))
		{
			ShowNotification("error", "Cannot add " + strType + ": " + strName + ", enemy or character with that name already exists!");
			return;
		}
		if(intMaxHP < 0 || intCurHP < 0)
		{
			ShowNotification("error", "Cannot add " + strType + ": " + strName + ", invalid maximum or current HP!");
		}
		if(intCurHP > intMaxHP)
			intCurHP = intMaxHP;
		var currentCharacters = GetCharacters();
		var ID = currentCharacters.length == 0 ? 1 : Math.max.apply(Math,currentCharacters.map(function(o){return o.ID;})) + 1;
		var character = { "ID": ID, "Name": strName, "CurrentHP": intCurHP, "MaxHP":intMaxHP, "IsNPC": isNPC, "IsEnemy": isEnemy, "IsHidden": isHidden, "IsActive": isActive, "TokenNumber": Number(tokenNumber), "Afflictions": [], "CurrentTurn": false };
		currentCharacters.push(character);
		SetCharacters(currentCharacters);
		return ID;
	}
	function ActivateCharacters()
	{
		ToggleActiveAllCharacters(true);
		DisplayDMArray(POPULATEID);
	}
	function InactivateCharacters()
	{
		ToggleActiveAllCharacters(false);
		DisplayDMArray(POPULATEID);
	}
	function ToggleActiveAllCharacters(makeActive)
	{
		var characters = GetCharacters();
		for(var i=0;i<characters.length;i++)
		{
			characters[i].IsActive = makeActive;
		}
		SetCharacters(characters);
	}
	function CharacterTurn(isNext)
	{
		var characters = GetCharacters();
		var nextActiveIndex = GetNextActiveIndex(-1, isNext, characters);
		if(nextActiveIndex == -1)
			return;
		var oldIndex = -1;
		for(var i = 0; i < characters.length; i++)
		{
			if(characters[i].CurrentTurn && i != nextActiveIndex)
			{
				characters[i].CurrentTurn = false;	
				oldIndex = i;
			}
			if(i == nextActiveIndex)
				characters[i].CurrentTurn = true;
		}
		if(oldIndex != -1)
		{
			if(isNext && oldIndex == characters.length - 1)
			{
				IncrementRound();
			}
			if(!isNext && oldIndex == 0)
			{
				DecrementRound();
			}
			SetRoundElement();
		}
		ScrollToElement("row" + characters[nextActiveIndex].ID, false);
		SetCharacters(characters);
		ShowNotification("notification", isNext ? "Next Turn" : "Previous Turn", false);
		AddMessageToQueue("", "", characters[nextActiveIndex].ID);
	}
	function SetCharacters(characterArray)
	{
		SetJsonStorageItem(CHARITEM, characterArray);
	}
	function SetMessages(messageArray)
	{
		SetJsonStorageItem(MSGITEM, messageArray);
	}