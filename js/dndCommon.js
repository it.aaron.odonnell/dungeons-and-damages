	$(document).ready(function(){
		ResetTooltip();
	});
	function ResetTooltip()
	{
		$('[data-toggle="tooltip"]').tooltip(); 
		$('[data-toggle="tooltipHTML"]').tooltip({html:true}); 
	}
    var CHARITEM = "characters";
	var MSGITEM = "messages";
	var POPULATEID = 'observe';
	var MSG_SPACE = 300;
	var SCROLLOFFSET = 200;
	var lsNotifyKey = "enableClientNotifications";
	var STARTUP_MESSAGE = "";
	var Afflictions = [
	{ Name: "Blinded", Tooltip:"Auto-fail Sight Ability Checks, Adv. Enemy ATK Rolls, Disadv. ATK Rolls", "By": false , "Bad": true},
	{ Name: "Charmed", Tooltip:"Cannot Attack Charmer, Charmer Adv Social Ability Checks", "By": false , "Bad": true},
	{ Name: "Deafened", Tooltip:"Auto-fail Hearing Ability Checks", "By": false , "Bad": true},
	{ Name: "Exhaustion 1", Tooltip:"Disadv. Ability Checks", "By": true , "Bad": true}, 
	{ Name: "Exhaustion 2", Tooltip:"Disadv. Ability Checks, Speed Halved", "By": true , "Bad": true},
	{ Name: "Exhaustion 3", Tooltip:"Disadv. Ability Checks, Speed Halved, Disadv. ATK Rolls/Saving Throws", "By": true , "Bad": true},
	{ Name: "Exhaustion 4", Tooltip:"Disadv. Ability Checks, Speed Halved, Disadv. ATK Rolls/Saving Throws, Half Max HP", "By": true , "Bad": true}, 
	{ Name: "Exhaustion 5", Tooltip:"Disadv. Ability Checks, Speed Zero, Disadv. ATK Rolls/Saving Throws, Half Max HP", "By": true , "Bad": true},
	{ Name: "Frightened", Tooltip:"Disadv. Ability Checks, Disadv. ATK Rolls whilst source within line of sight, Cannot willingly move closer to source", "By": false , "Bad": true},
	{ Name: "Grappled", Tooltip:"Speed=0, No Speed Bonuses, STR Check Break", "By": false , "Bad": true}, 
	{ Name: "Invisible", Tooltip:"Disadv. Enemy ATK Rolls, Adv. ATK Rolls", "By": false , "Bad": true},
	{ Name: "Paralyzed", Tooltip:"Cannot Move/Speak/Action/Reaction, Auto-fail STR/DEX Saves, Adv. Enemy ATK Rolls, Auto Crit Enemy ATK within 5ft", "By": false , "Bad": true},
	{ Name: "Petrified", Tooltip:"Solid Aninimate Substance, Cannot Move/Speak, Unaware Surroundings, Auto-fail STR/DEX Saves, All Damage Resistance, Immune to Poison/Disease, Suspend Existing Poison/Disease", "By": false , "Bad": true}, 	
	{ Name: "Poisoned", Tooltip:"Disadv. ATK Rolls/Ability Checks", "By": false , "Bad": true}, 
	{ Name: "Prone", Tooltip:"Standup to end or crawl, Disadv. ATK Rolls, Enemy Adv. ATK Rolls <= 5ft, Enemy Disadv. ATK Rolls > 5ft", "By": false , "Bad": true}, 
	{ Name: "Restrained", Tooltip:"Speed=0, No Speed Bonuses, Enemy Adv. ATK Rolls, Disadv. ATK Rolls, Disadv. DEX Saving Throws", "By": false , "Bad": true}, 
	{ Name: "Stunned", Tooltip:"Cannot Move, Speak Falteringly, Auto-fail STR/DEX Saving Throws, Enemy Adv. ATK Rolls", "By": false , "Bad": true},
	{ Name: "Unconscious", Tooltip:"Cannot Move/Speak, Unaware of Surroundings, Drop held Items, Auto-fail STR/DEX Saving Throws, Enemy Adv. ATK Rolls, Enemy Auto-crit within 5ft", "By": false , "Bad": true},
	];
	var UnconsciousAffliction = Afflictions[Afflictions.length - 1];
	var CHARNAMEPLACEHOLDER = "charNamePlaceHolder";
	function Initialise(elmID, notyTimeout)
	{
		POPULATEID = elmID;
		MSG_SPACE = notyTimeout;
		InitialiseViewerNotifications();
	}
	function ClosePreloader() {
		$("#status").delay(1500).fadeOut("slow");
		$("#preloader").delay(2000).fadeOut("slow");
	};
	function GetCharactersByNamePassArray(strName, characters)
	{
		var result = characters.filter(function( obj ) {
		  return obj.Name == strName;
		});
		return result;
		
	}
	function GetCharactersByIDPassArray(intID, characters)
	{
		var result = characters.filter(function( obj ) {
		  return obj.ID == intID;
		});
		return result;
		
	}
	function GetIndexOfCharacterByNamePassArray(strName, characters)
	{
		for(var i = 0; i < characters.length; i++)
		{
			if(characters[i].Name == strName)
				return i;
		}
		return -1;
		
	}
	function GetIndexOfCharacterByIDPassArray(strID, characters)
	{
		var intID = Number(strID);
		for(var i = 0; i < characters.length; i++)
		{
			if(characters[i].ID == intID)
				return i;
		}
		return -1;
	}
	function GetCurrentRowNumber()
	{
		var currentlySelectedRow = $("tr.current");
		if(!currentlySelectedRow)
		{
			//No row selected.
			return 0;
		}
		return $("tr.current").index();
	}
	function GetSingleActiveCharacterIndex(characters)
	{
		var result = characters.filter(function( obj ) {
		  return obj.IsActive == true;
		});
		if(result.length == 0)
			return -1;
	    if(result.length > 1)
			return -2;
		return GetIndexOfCharacterByIDPassArray(result[0].ID, characters);
	}
	//2, next active is 1, length is 3
	function GetNextActiveIndex(currentNum, isNext, characters)
	{
		if(currentNum == -1)
		{
			var charIndex = GetSingleActiveCharacterIndex(characters); //no active = -1, more than 1 active = -2
			if(charIndex == -1)
				return -1;
			if(charIndex != -2)
				return charIndex;
			currentNum = GetIndexOfCurrentTurnPassArray(characters);
			if(currentNum == -1)
				return 0;
		}
		if(isNext)
		{
			if(currentNum + 1 == characters.length)
				currentNum = 0;
			else
				currentNum = currentNum + 1;
		}
		else
		{
			if(currentNum == 0)
				currentNum = characters.length - 1;
			else
				currentNum = currentNum - 1;
		}
		if(characters[currentNum].IsActive)
			return currentNum;
		return GetNextActiveIndex(currentNum, isNext, characters);			
	}
	function GetCharactersByNamePassArray(strName, characters)
	{
		var result = characters.filter(function( obj ) {
		  return obj.Name == strName;
		});
		return result;
		
	}
	function GetCharacters()
	{
		var currentCharacters = GetJsonStorageItem(CHARITEM);
		if(currentCharacters == undefined ||  !(currentCharacters instanceof Array))
		{
			currentCharacters = [ ];
		}
		return currentCharacters;
	}
	function InitialiseViewerNotifications()
	{
		var notify = GetStorageItem(lsNotifyKey);
		if(notify == null)
			SetStorageItem(lsNotifyKey, "true");
	}
	function GetMessages()
	{
		var currentMessages = GetJsonStorageItem(MSGITEM);
		if(currentMessages == undefined ||  !(currentMessages instanceof Array))
		{
			currentMessages = [ ];
		}
		return currentMessages;
	}
	function GetNotificationsAreAllowed()
	{
		var notify = GetStorageItem(lsNotifyKey);
		return notify == "true";
	}
	function GetIndexOfCurrentTurn()
	{
		var characters = GetCharacters();
		return GetIndexOfCurrentTurnPassArray(characters);
	}
	function GetIndexOfCurrentTurnPassArray(characters)
	{
		for(var i = 0; i < characters.length; i++)
		{
			if(characters[i].CurrentTurn == true)
				return i;
		}
		return -1;
	}
	function GetCharactersByID(intID)
	{
		var characters = GetCharacters();
		return GetCharactersByIDPassArray(intID, characters);
	}
	function GetCharactersByName(strName)
	{
		var characters = GetCharacters();
		return GetCharactersByNamePassArray(strName, characters);
	}
	function GetCharacterByID(intID)
	{
		var result = GetCharactersByID(intID);
		if(result.length == 0)
			return null;
		return result[0];
	}
	function GetCharacterByName(strName)
	{
		var result = GetCharactersByName(strName);
		if(result.length == 0)
			return null;
		return result[0];
	}
	function GetCharacterByNamePassArray(strName, characters)
	{
		var result = GetCharactersByNamePassArray(strName, characters);
		if(result.length == 0)
			return null;
		return result[0];
	}
	//Single Character Functions
	function CharacterExists(strName)
	{
		return GetCharacterByName(strName) != null;
	}
	var confirmVal = false;
	function NotyConfirm(strMessage, trueCbFn, falseCbFn)
	{
		confirmVal = false;
		var n = new Noty({
		  text: strMessage,
		  buttons: [
			Noty.button('YES', 'btn btn-success', function(){trueCbFn(); n.close();}),
			Noty.button('NO', 'btn btn-error', function(){falseCbFn(); n.close();})
		  ]
		}).show();
	}
  function GetIndexesByCharacterNamePassArray(firstStrName, secondStrName, characters)
  {
	  var indexLocations = [-1, -1];
	  for(var i = 0; i < characters.length; i++)
	  {
		  if(characters[i].Name == firstStrName)
		  {
			  indexLocations[0] = i;
		  }
		  if(characters[i].Name == secondStrName)
		  {
			  indexLocations[1] = i;
		  }
	  }
	  return indexLocations;
  }
  function GetIndexesByCharacterIDPassArray(firstID, secondID, characters)
  {
	  var indexLocations = [-1, -1];
	  for(var i = 0; i < characters.length; i++)
	  {
		  if(characters[i].ID == firstID)
		  {
			  indexLocations[0] = i;
		  }
		  if(characters[i].ID == secondID)
		  {
			  indexLocations[1] = i;
		  }
	  }
	  return indexLocations;
  }
	//Storage Functions
	function StorageJSONToHTML(elmID, key)
	{
		$("#" + elmID).html(GetStorageItem(key));
	}
	function SetStorageItem(key, val)
	{
		localStorage.setItem(key, val);
	}
	function GetStorageItem(key)
	{
		return localStorage.getItem(key);
	}
	function SetJsonStorageItem(key, obj)
	{
		SetStorageItem(key, JSON.stringify(obj));
	}
	function GetJsonStorageItem(key)
	{
		return JSON.parse(GetStorageItem(key));
	}
	function ClearLocalStorageItem(key)
	{
		localStorage.removeItem(key);
	}
	function ClearAllLocalStorage()
	{
		NotyConfirm("Are you sure you want to clear the storage?", function() {localStorage.clear();}, function(){});
	}
	function ShowNotification(type,message,isViewer)
	{
		if(!message)
			return;
		new Noty({
				type: type,
				text: "<span style='font-size:" + (isViewer ? "36pt" : "20pt") + ";'>" + message + "</span>",
				layout: (isViewer ? 'center' : 'topRight'),
				timeout: 1000,
			}).show();  		
	}
	function ScrollToCurrentTurn()
	{
		var characters = GetAvailableCharacterArray();
		var currentCharacters = characters.filter(function( obj ) {
		  return obj.CurrentTurn == true;
		});
		if(currentCharacters.length == 0)
			return;
		ScrollToElement("row" + currentCharacters[0].ID, true);
	}
	//Display Functions
	function GetAvailableCharacterArray()
	{
		var characters = GetCharacters();
		characters = characters.filter(function( obj ) {
		  return obj.IsActive == true;
		});
		var isSet = false;
		for(var i = 0; i < characters.length; i++)
		{
			characters[i].Name = GetAllowedName(characters[i]);
			if(characters[i].IsEnemy)
			{
				characters[i].CurrentHP = '??';
				characters[i].MaxHP = '??';
			}
			if(characters[i].CurrentTurn)
				isSet = true;
		}
		if(!isSet && characters.length > 0)
			characters[0].CurrentTurn = true;
		return characters;
	}
	function SetXPElement()
	{
		$("#displayXP").val(GetXP());
	}
	function SetRoundElement()
	{
		$("#displayRound").val(GetRound());
		SetSecondsElement();
	}
	function SetSecondsElement()
	{
		$("#displayMinutesAndSeconds").html(GetMinutesAndSeconds((GetRound() - 1) * 6));
	}
	function GetMinutesAndSeconds(timeSeconds)
	{
		var minutes = Math.floor(timeSeconds / 60);
		var seconds = timeSeconds - minutes * 60;
		return minutes + "m " + seconds + "s";
	}
	function GetAllowedName(character)
	{
		if(character.IsHidden)
			return "???";
		return character.Name;
	}
	function GetRound()
	{
		var strRoundNum = GetStorageItem("Round");
		var currentRound = 1;
		if(strRoundNum)
			currentRound = Number(strRoundNum);
		return currentRound;
	}
	function ScrollToElement(elmID, isViewer) {
		var elm = $("#" + elmID);
		if(elm == null)
			return;
		if(isViewer)
		{
			if(elm.offset().top < SCROLLOFFSET)
			{
				$('html, body').animate({
					scrollTop: 0
				}, 'fast');	
			}
			else
			{
				$('html, body').animate({
					scrollTop: elm.offset().top - SCROLLOFFSET
				}, 'fast');	
			}
		}
		else
		{
			if(elm[0] != null)
			{
				elm[0].scrollIntoView(true);
				$("#" + elmID).scrollTop -= SCROLLOFFSET;
			}
		}
	}
	function closeNav() {
		$(".sidenav").removeClass("opened");
		$(".pushcontent").removeClass("pushed");
	}
	
	function openNav() {
		$(".sidenav").addClass("opened");
		$(".pushcontent").addClass("pushed");
	}

	function toggleNav() {
		$(".sidenav").toggleClass("opened");
		$(".pushcontent").toggleClass("pushed");
	}